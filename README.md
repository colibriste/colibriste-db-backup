# Colibriste-backup

## Description
This project has for goal to make postgres DB dumps at regular times and clean old dumps.

The project is containerized for easier deployment.

## Roadmap

Add several db engine motor. The first one will be MariaDB.

## License
This project is provided under the MIT Licence.

