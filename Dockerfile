FROM alpine:3.18

# Add bash
RUN apk add --no-cache bash
# Add PostgreSQL client
RUN apk add --no-cache postgresql-client
# Add findutils to use 'printf' option on 'find' command
RUN apk add --no-cache findutils

# Set timezone to Paris
RUN apk add --no-cache tzdata
ENV TZ=Europe/Paris

# Add DUMP shell script
COPY dump.sh /postgresql-client/
RUN chmod 0777 /postgresql-client/dump.sh

# Copy cronfile into container
COPY backup-cron /etc/crontab
# Execute crontab to take cronfile into consideration
RUN crontab /etc/crontab
# Create cron file log
RUN touch /var/log/cron.log

# Start container with crontab
CMD crond -f