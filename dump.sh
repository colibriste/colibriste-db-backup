#!/bin/bash

# Beginning message
echo 'BACKUP START...'

PERIODICITY=$1
NUMBER_TO_KEEP=$2
DATE=$(date +%Y-%m-%d-%Hh-%Mmn-%Ss)
ROOT_FOLDER_NAME="/dumps" # Backuped PVC is mounted here
FOLDER_NAME="${ROOT_FOLDER_NAME}/$PERIODICITY-$DATE"
FOLDER_NAME_PATTERN="$PERIODICITY-*"
DB_PORT=5432

# debug
echo
echo "--- Environment variables ---"
echo DB_PORT             = $DB_PORT
echo
echo CLOUD_HOST          = $CLOUD_HOST
echo CLOUD_DB            = $CLOUD_DB
echo CLOUD_USER_NAME     = $CLOUD_USER_NAME
echo
echo RSS_HOST            = $RSS_HOST
echo RSS_DB              = $RSS_DB
echo RSS_USER_NAME       = $RSS_USER_NAME
echo
echo WIKI_HOST           = $WIKI_HOST
echo WIKI_DB             = $WIKI_DB
echo WIKI_USER_NAME      = $WIKI_USER_NAME
echo
echo CONNECT_HOST        = $CONNECT_HOST
echo CONNECT_DB          = $CONNECT_DB
echo CONNECT_USER_NAME   = $CONNECT_USER_NAME
echo
echo MAIL_HOST           = $MAIL_HOST
echo MAIL_DB             = $MAIL_DB
echo MAIL_USER_NAME      = $MAIL_USER_NAME
echo
echo DOLIBARR_HOST       = $DOLIBARR_HOST
echo DOLIBARR_DB         = $DOLIBARR_DB
echo DOLIBARR_USER_NAME  = $DOLIBARR_USER_NAME
echo
echo INVIDIOUS_HOST      = $INVIDIOUS_HOST
echo INVIDIOUS_DB        = $INVIDIOUS_DB
echo INVIDIOUS_USER_NAME = $INVIDIOUS_USER_NAME
echo
echo PERIODICITY         = $PERIODICITY
echo NUMBER_TO_KEEP      = $NUMBER_TO_KEEP
echo
echo DATE                = $DATE
echo
echo ROOT_FOLDER_NAME    = $ROOT_FOLDER_NAME
echo FOLDER_NAME         = $FOLDER_NAME
echo FOLDER_NAME_PATTERN = $FOLDER_NAME_PATTERN

# Create a new directory into backup directory location for this date
mkdir -p $FOLDER_NAME

# Dump the databases
echo
echo "--- Dump ---"

export PGPASSWORD="$CLOUD_USER_PASSWORD"
pg_dump --file "$FOLDER_NAME/$CLOUD_DB.sql" --host "$CLOUD_HOST" --port "$fDB_PORT" --username "$CLOUD_USER_NAME" --format=t --blobs --schema "public" "$CLOUD_DB"

export PGPASSWORD="$RSS_USER_PASSWORD"
pg_dump --file "$FOLDER_NAME/$RSS_DB.sql" --host "$RSS_HOST" --port "$fDB_PORT" --username "$RSS_USER_NAME" --format=t --blobs --schema "public" "$RSS_DB"

export PGPASSWORD="$WIKI_USER_PASSWORD"
pg_dump --file "$FOLDER_NAME/$WIKI_DB.sql" --host "$WIKI_HOST" --port "$fDB_PORT" --username "$WIKI_USER_NAME" --format=t --blobs --schema "public" "$WIKI_DB"

export PGPASSWORD="$CONNECT_USER_PASSWORD"
pg_dump --file "$FOLDER_NAME/$CONNECT_DB.sql" --host "$CONNECT_HOST" --port "$fDB_PORT" --username "$CONNECT_USER_NAME" --format=t --blobs --schema "public" "$CONNECT_DB"

export PGPASSWORD="$MAIL_USER_PASSWORD"
pg_dump --file "$FOLDER_NAME/$MAIL_DB.sql" --host "$MAIL_HOST" --port "$fDB_PORT" --username "$MAIL_USER_NAME" --format=t --blobs --schema "public" "$MAIL_DB"

export PGPASSWORD="$DOLIBARR_USER_PASSWORD"
pg_dump --file "$FOLDER_NAME/$DOLIBARR_DB.sql" --host "$DOLIBARR_HOST" --port "$fDB_PORT" --username "$DOLIBARR_USER_NAME" --format=t --blobs --schema "public" "$DOLIBARR_DB"

export PGPASSWORD="$INVIDIOUS_USER_PASSWORD"
pg_dump --file "$FOLDER_NAME/$INVIDIOUS_DB.sql" --host "$INVIDIOUS_HOST" --port "$fDB_PORT" --username "$INVIDIOUS_USER_NAME" --format=t --blobs --schema "public" "$INVIDIOUS_DB"

echo $FOLDER_NAME content :
ls -al $FOLDER_NAME

# Remove all but last n backups
echo
echo "--- Existing backups ---"
ls -al $ROOT_FOLDER_NAME

echo
echo "--- Removing old backups ---"
echo | find $ROOT_FOLDER_NAME/* -maxdepth 1 -type d -name $FOLDER_NAME_PATTERN |
    sort -t $'\t' -g |
    head -n -$NUMBER_TO_KEEP |
    cut -d $'\t' -f 2-

find $ROOT_FOLDER_NAME/* -maxdepth 1 -type d -name $FOLDER_NAME_PATTERN -printf '%T@\t%p\n' |
    sort -t $'\t' -g |
    head -n -$NUMBER_TO_KEEP |
    cut -d $'\t' -f 2- |
    xargs rm -Rf


# Final message
echo
echo 'BACKUPS DONE.'
